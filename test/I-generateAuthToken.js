const chai = require('chai'),
    chaiHttp = require('chai-http'),
    config = require("../config");
const jwt = require('jsonwebtoken');

chai.use(chaiHttp);

const bcrypt = require('bcrypt');
chai.should();

let {MongoMemoryServer} = require("mongodb-memory-server");
let mongoose = require('mongoose');

mongoose.Promise = Promise;

require("../mongooseModels");

config.port = false;
let app = require("../server");

let mongoServer;
describe('Account', function () {
    before(async function () {
        this.timeout(60e3);

        console.log("setup database");
        mongoServer = new MongoMemoryServer();

        let mongoConnectionString = await mongoServer.getConnectionString();
        mongoose.connect(mongoConnectionString, {useNewUrlParser: true});

        await new Promise((resolve) => {
            mongoose.connection.on('error', async (e) => {
                if (e.message.code === 'ETIMEDOUT') {
                    console.log(e);
                    mongoose.connect(mongoConnectionString, {useNewUrlParser: true});
                }
                console.log(e);
            });

            mongoose.connection.once('open', () => {
                console.log(`MongoDB successfully connected to ${mongoConnectionString}`);
                resolve();
            });

        });

        await (new mongoose.model("AdminUser")({
            Title: 'Principal',
            Name: 'Test user',
            UserName: 'ben',
            Password: bcrypt.hashSync("password", 5),
            Token: '',
            Attempts: [],
            Active: 1
        })).save();

        await (new mongoose.model("AdminUser")({
            Title: 'administration',
            Name: 'Test user',
            UserName: 'bp',
            Password: bcrypt.hashSync("password", 5),
            Token: '',
            Attempts: [],
            Active: 0
        })).save();

    });

    after(async function () {
        await mongoServer.stop();
    });

    describe("login", function () {
        describe("validation", function () {
            it("Reject missing data", async function () {
                let data = await chai.request(app).post("/token").send();
                data.body.error.should.be.eql("'grant_type' has to be provided and be strings")
            });

            it("Reject wrong grant_type data", async function () {
                let data = await chai.request(app).post("/token").send({grant_type: "blah blah blah"});
                data.body.error.should.be.eql("'grant_type' has to be 'password' or 'refresh_token'")
            });

            it("Reject wrong only password as type data", async function () {
                let data = await chai.request(app).post("/token").send({grant_type: "password"});
                data.body.error.should.be.eql("'username' and 'password' has to be provided and be strings")
            });

            it("Reject wrong type on password login", async function () {
                let data = await chai.request(app).post("/token").send({
                    grant_type: "password",
                    username: [1, 2],
                    password: "test"
                });
                data.body.error.should.be.eql("'username' and 'password' has to be provided and be strings")
            });

            it("Reject wrong type on refresh_token login", async function () {
                let data = await chai.request(app).post("/token").send({
                    grant_type: "refresh_token",
                    refresh_token: [1, 2]
                });
                data.body.error.should.be.eql("'refresh_token' has to be provided and be strings")
            })
        });

        it("logs in", async function () {
            let data = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "password"
            });

            (typeof data.body.error).should.be.eql("undefined");
            data.body.token_type.should.be.eql("bearer");
            data.body.expires_in.should.be.eql(15);
            data.body.refresh_token.should.contain(".");
            data.body.access_token.should.be.a("string");

            (function () {
                jwt.verify(data.body.access_token, "rtyghf");
            }).should.throw("invalid signature");

            let returnedToken = jwt.verify(data.body.access_token, config.jwtSecret);

            returnedToken.Name.should.be.eql("Test user");
            returnedToken.UserName.should.be.eql("ben");
            returnedToken.aud.should.be.eql("Principal");
            returnedToken.iat.should.be.approximately(Math.round(Date.now() / 1000), 5);
            returnedToken.exp.should.be.approximately(Math.round(Date.now() / 1000) + 60 * 15, 5);
        });

        it("rejects wrong user", async function () {
            let data = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "joe",
                password: "password"
            });

            data.body.error.should.be.eql("Wrong username or password");
        });

        it("rejects deactivated user", async function () {
            let data = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "bp",
                password: "password"
            });

            data.body.error.should.be.eql("Wrong username or password");
        });

        it("rejects wrong password", async function () {
            let data = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "not the password"
            });

            data.body.error.should.be.eql("Wrong username or password");

            (await mongoose.model("AdminUser").findOne({UserName: "ben"}))
                .Attempts[0].should.be.approximately(Date.now(), 5000);
        });

        it("rejects to many attempts", async function () {
            for (let i = 0; i < 2; i++) {
                let data = await chai.request(app).post("/token").send({
                    grant_type: "password",
                    username: "ben",
                    password: "not the password"
                });

                data.body.error.should.be.eql("Wrong username or password");
            }

            let data = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "password"
            });

            data.body.error.should.be.eql("To many attempts");
        });

        it("allows access again", async function () {
            let user = await mongoose.model("AdminUser").findOne({UserName: "ben"});
            user.Attempts = user.Attempts.map(i => i - (1e3 * 60 * 60 * 5));
            await user.save();

            let data = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "password"
            });

            (typeof data.body.error).should.be.eql("undefined");

            user.Attempts = [];
            await user.save();
        });
    });


    describe("refresh token", function () {
        it("Gives a new token", async function () {
            let oldToken = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "password"
            });

            (typeof oldToken.body.error).should.be.eql("undefined");

            await new Promise(resolve => setTimeout(resolve, 1000));

            let newToken = await chai.request(app).post("/token").send({
                grant_type: "refresh_token",
                refresh_token: oldToken.body.refresh_token
            });

            (typeof newToken.body.error).should.be.eql("undefined");
            oldToken.body.access_token.should.not.be.eql(newToken.body.access_token);
            oldToken.body.refresh_token.should.not.be.eql(newToken.body.refresh_token)
        });

        it("Reject wrong format", async function () {
            let oldToken = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "password"
            });

            (typeof oldToken.body.error).should.be.eql("undefined");


            let newToken = await chai.request(app).post("/token").send({
                grant_type: "refresh_token",
                refresh_token: oldToken.body.refresh_token.replace(".", "")
            });

            newToken.body.error.should.be.eql("Wrong token format");
        });

        it("Reject wrong user", async function () {
            let oldToken = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "password"
            });

            (typeof oldToken.body.error).should.be.eql("undefined");


            let newToken = await chai.request(app).post("/token").send({
                grant_type: "refresh_token",
                refresh_token: "not" + oldToken.body.refresh_token
            });

            newToken.body.error.should.be.eql("Failed to refresh the token");
        });

        it("Reject wrong token", async function () {
            let oldToken = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "password"
            });

            (typeof oldToken.body.error).should.be.eql("undefined");


            let newToken = await chai.request(app).post("/token").send({
                grant_type: "refresh_token",
                refresh_token: oldToken.body.refresh_token + "not"
            });

            newToken.body.error.should.be.eql("Failed to refresh the token");
        });

        it("Reject disabled user token", async function () {
            let oldToken = await chai.request(app).post("/token").send({
                grant_type: "password",
                username: "ben",
                password: "password"
            });

            (typeof oldToken.body.error).should.be.eql("undefined");

            let user = await mongoose.model("AdminUser").findOne({UserName: "ben"});
            user.Active = 0;
            await user.save();

            let newToken = await chai.request(app).post("/token").send({
                grant_type: "refresh_token",
                refresh_token: oldToken.body.refresh_token + "not"
            });

            newToken.body.error.should.be.eql("Failed to refresh the token");
        })

    });
});
