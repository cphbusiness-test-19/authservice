let bcrypt = require("bcrypt");
let mongoose = require('mongoose');
let config = require("./config");
var jwt = require('jsonwebtoken');
let crypto = require('crypto');

require("./mongooseModels");

function toManyAttempts(user) {
    if (user.Attempts.length < 3) return false;
    return user.Attempts[2] > Date.now() - 4 * 60 * 1e3; // 4 hours
}

async function login(username, password) {
    let user = await mongoose.model("AdminUser").findOne({UserName: username, Active: 1});
    if (user === null) return {error: "Wrong username or password"};


    if (toManyAttempts(user)) return {error: "To many attempts"};

    if (!await bcrypt.compare(password, user.Password)) {
        user.Attempts.unshift(Date.now());
        await user.save();
        return {error: "Wrong username or password"}
    }

    user.Token = crypto.randomBytes(64).toString('hex');
    await user.save();

    return {
        token_type: "bearer",
        access_token: jwt.sign(
            {
                Name: user.Name,
                UserName: user.UserName
            },
            config.jwtSecret,
            {expiresIn: 60 * 15, audience: user.Title}
        ),
        expires_in: 15,
        refresh_token: user.UserName + "." + user.Token
    }

}

async function refreshToken(refreshToken) {
    let [userName, token] = refreshToken.split(".");
    if(typeof userName === "undefined" || typeof token === "undefined") return {error: "Wrong token format"};

    let user = await mongoose.model("AdminUser").findOne({UserName: userName, Token: token, Active: 1});
    if (user === null) return {error: "Failed to refresh the token"};

    user.Token = crypto.randomBytes(64).toString('hex');
    await user.save();

    return {
        token_type: "bearer",
        access_token: jwt.sign(
            {
                Name: user.Name,
                UserName: user.UserName
            },
            config.jwtSecret,
            {expiresIn: 60 * 15, audience: user.Title}
        ),
        expires_in: 15,
        refresh_token: user.UserName + "." + user.Token
    }
}


module.exports = {login, refreshToken};