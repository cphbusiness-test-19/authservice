let config = require("./config");
let express = require("express");
let bodyParser = require("body-parser");
let mongoose = require('mongoose');
let auth = require("./auth");

mongoose.Promise = Promise;

/* istanbul ignore next */
if (config.databaseurl !== false) {
    mongoose.connect(config.databaseurl, {useNewUrlParser: true});
}

let app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


app.post("/token", async (req, res) => {
    if(typeof req.body.grant_type != "string") return res.json({
        error: "'grant_type' has to be provided and be strings"
    });
    switch (req.body.grant_type) {
        case "password":
            if (typeof req.body.username != "string" || typeof req.body.password != "string")
                return res.json({
                    error: "'username' and 'password' has to be provided and be strings"
                });

            res.json(await auth.login(req.body.username, req.body.password));
            return;

        case "refresh_token":
            if(typeof req.body.refresh_token != "string") return res.json({
                error: "'refresh_token' has to be provided and be strings"
            });
            res.json(await auth.refreshToken(req.body.refresh_token));
            return

    }

    res.json({
        error: "'grant_type' has to be 'password' or 'refresh_token'"
    });
});

/* istanbul ignore next */
if (config.port !== false) {
    app.listen(config.port, _ => console.log(`Server is running on port ${config.port}`));
}

module.exports = app;