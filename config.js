module.exports = {
    port: parseInt(process.env.port || 80),
    databaseurl: process.env.databaseurl || false,
    jwtSecret: process.env.jwtSecret
};