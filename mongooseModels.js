const mongoose = require('mongoose');

mongoose.model('AdminUser', {
    Title: String,
    Name: String ,
    UserName: String ,
    Password: String,
    Token: String ,
    Attempts: Array,
    Active: Number
});

