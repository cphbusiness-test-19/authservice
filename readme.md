# Auth server

## Api

### Login - POST _/token_

Request

```
{
    "grant_type": "password",
    "username": "user",
    "password": "password"
}
```

Response #1 - 200

```
{
    "error": "error message"
}
```

Response #2 - 200

```
{
    "token_type": "bearer",
    "access_token": ".................",
    "expires_in": 15,
    "refresh_token": "................."
}
```

### Refresh token - POST _/token_

Request #1

```
{
    "grant_type": "refresh_token",
    "refresh_token": "logintoken.refresh_token"
}
```

Request #2

```
{
    "grant_type": "refresh_token",
    "refresh_token": "refreshtoken.refresh_token"
}
```

Response #1 - 200

```
{
    "error": "error message"
}
```

Response #2 - 200

```
{
    "token_type": "bearer",
    "access_token": ".................",
    "expires_in": 15,
    "refresh_token": "................."
}
```

